# T1GER Mix Bundle

This package contains 15 premium resources worth all together: £400+

All code is available

There is no support at all offered. These resources are not maintained any more and deprecated.

## Support
The resources in this bundle are officially not maintained anymore. Therefore there are absolutely no support.
Resources in this package are all deprecated. Use at own risk

## Contributing
- You are more than welcome to contribute to this project.
- It needs support for QB Core!

## Authors and acknowledgment
- T1GER_Scripts